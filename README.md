# JavaBilloCustomersToExcelXml
Immature. Do not use.

This is a no-frills exporter that fetches Billomat customers via API and produces XML that can be read
by Excel, OpenOffice, and LibreOffice. It's written in Java and has a simple GUI.

This project is meant to be implemented into some larger software. It is not a standalone program.

# Author

Me:

1. https://reneschmidt.de/wiki/index.java/page/view/JavaBilloCustomersToExcelXml,Start
2. https://reneschmidt.de/
3. [I am available for hire](mailto:wiki@reneschmidt.de)

# Licence

GPL v3 or commercial licence :) from github@reneschmidt.de. Do not use this in your closed source project
without paying me. I don't like that.

# Source/Download

[Source can be found at BitBucket](https://bitbucket.org/r_schmidt/javabillocustomerstoexcelxml/src)

# Requirements

1. Java (tested with Java 7)
2. Apache HttpComponents
3. Apache Commons Lang
4. Apache Commons IO
5. Excel (tested with 2007, should work with 2003 and newer versions as well)
6. or OpenOffice/LibreOffice (tested with 4.x)

# How to use

Install requirements. For Ubuntu this would be

```bash
 sudo apt-get install libcommons-lang3-java libcommons-io-java libhttpclient-java
```

Get a Billomat API key:

1. Log into your Billomat account.
2. In the page header, click your user name.
3. Click "API-Schlüssel anzeigen", write it down along with your Billomat ID.

Then run CustomerExporter.

The program will ask you for your Billomat ID and API key and then save your Billomat customers to an XML file.

Neither Billomat ID nor API key will be stored.

The Swing "GUI" has been made for educational purposes only (*my* education...).

Please note that you will need to open your spreadsheet program and "manually" open the file as XML files usually
are not associated with spreadsheet programs so you most probably cannot just double-click the file to open it
(in a spreadsheet that is).