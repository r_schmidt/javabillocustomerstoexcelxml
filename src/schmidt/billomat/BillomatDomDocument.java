package schmidt.billomat;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import org.apache.commons.lang3.StringEscapeUtils;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

// For transformation
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamSource;

// for traversing
import javax.xml.xpath.*;

import org.w3c.dom.Document;

/**
 * Billomat Document
 */
public class BillomatDomDocument {
  /**
   * XML in string representation
   */
  public String xmlString;

  /**
   * XML document
   */
  public Document xmlDoc;

  /**
   * Client nodes
   */
  public NodeList clientNodes;

  /**
   * Constructor
   *
   * @param xmlString XMl in String representation
   */
  public BillomatDomDocument(String xmlString) {
    this.xmlString = xmlString;
  }

  /**
   * Enrich document with customer attributes
   *
   * @param bmReq   Billomat Request object
   * @param progMon Progress monitor
   */
  public void enrichDocWithCustomerAttribs(BillomatRequest bmReq, JilloMateProgressMonitor progMon) {
    try {
      XPath xpath = XPathFactory.newInstance().newXPath();
      XPathExpression idExpr = xpath.compile("id"); // The new xpath expression to find 'id' within 'client'

      for (int i = 0; i < clientNodes.getLength(); i++) {
        progMon.setNote("Hole Attribute...");
        progMon.increment();

        Node clientNode = clientNodes.item(i);
        if (clientNode != null && clientNode.getNodeType() == Node.ELEMENT_NODE) {
          Element clientElement = (Element) clientNode;
          NodeList idNodes = (NodeList) idExpr.evaluate(clientElement, XPathConstants.NODESET); // find id node

          Node idNode = idNodes.item(0);

          enrichNodeWithCustomerAttribs(bmReq, idNode);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Enrich single client node with attributes
   *
   * @param bmReq  Billomat requester
   * @param idNode ID node
   */
  public void enrichNodeWithCustomerAttribs(BillomatRequest bmReq, Node idNode) {
    BillomatRequest attribReq = new BillomatRequest(bmReq.apiKey, bmReq.billomatId);

    try {
      String clientId = idNode.getTextContent();
      attribReq.doRequest("/api/client-property-values?client_id=" + clientId, false);

      BillomatDomDocument xmlPropDoc = new BillomatDomDocument(attribReq.content);
      xmlPropDoc.parse();

      XPath xpath = XPathFactory.newInstance().newXPath();
      XPathExpression nameExpr = xpath.compile("name"); // The new xpath expression to find 'id' within 'client'

      NodeList propNodes = (NodeList) xpath.compile("/client-property-values/client-property-value").evaluate(xmlPropDoc.xmlDoc, XPathConstants.NODESET);

      Node clientNode = idNode.getParentNode();

      clientNode.appendChild(makeAttr(xmlDoc, "id", clientId));

      for (int i = 0; i < propNodes.getLength(); i++) {
        Node propNode = propNodes.item(i);
        if (propNode != null && propNode.getNodeType() == Node.ELEMENT_NODE) {
          Element clientElement = (Element) propNode;
          NodeList nameNodes = (NodeList) nameExpr.evaluate(clientElement, XPathConstants.NODESET); // find id node

          Node nameNode = nameNodes.item(0);
          String nameNodeString = xmlPropDoc.getValidNodeName(nameNode.getTextContent());
          Node newNode = clientNode.appendChild(xmlDoc.createElement(nameNodeString));

          Node valueNode = nameNode.getNextSibling();

          newNode.setTextContent(getNodeValueOrDefault(valueNode.getTextContent(), "-"));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      attribReq.getConnectionManager().shutdown();
    }
  }

  /**
   * Create node attribute.
   * This little method avoids hinting errors AND is legal to use.
   *
   * @param name Name of attribute
   * @param val  Value of attribute
   * @return DOMAttr
   */
  public Attr makeAttr(Document doc, String name, String val) {
    Attr xmlAttr = doc.createAttribute(name);
    xmlAttr.setNodeValue(val);

    return xmlAttr;
  }

  /**
   * Replace all chars that would make an invalid xml element name
   *
   * @param str String to manipulate
   * @return string
   */
  public String getValidNodeName(String str) {
    return str.toLowerCase().replaceAll("[^a-zA-Z-]", "-").replaceAll("--", "-");
  }

  /**
   * Check if val is empty, if so, return default value. if not, return val
   *
   * @param val        Value
   * @param defaultVal Default value
   * @return string
   */
  public String getNodeValueOrDefault(String val, String defaultVal) {
    if (val.equals("")) {
      return defaultVal;
    }

    return StringEscapeUtils.unescapeHtml4(val);
  }

  /**
   * Parse xml string into Document
   *
   * @return Document
   */
  public BillomatDomDocument parse() {
    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    builderFactory.setValidating(false); // and validating parser features
    builderFactory.setIgnoringElementContentWhitespace(true);

    DocumentBuilder builder;

    try {
      builder = builderFactory.newDocumentBuilder(); // Create the parser
      xmlDoc = builder.parse(new InputSource(new StringReader(xmlString)));

      XPath xpath = XPathFactory.newInstance().newXPath();
      clientNodes = (NodeList) xpath.compile("/clients/client").evaluate(xmlDoc, XPathConstants.NODESET);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return this;
  }

  /**
   * Convert input stream into string
   *
   * @param inputStream Input stream
   * @return String
   */
  public String convertInputStreamToString(InputStream inputStream) {
    String str = "";

    try {
      StringWriter writer = new StringWriter();
      IOUtils.copy(inputStream, writer, "UTF-8");
      str = writer.toString();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return str;
  }

  /**
   * Transform XMl according to XSL stylesheet and save to xmlFileName
   *
   * @param xslFileName XSL stylesheet file name, full path
   * @param xmlFileName XML target file name, full path
   */
  public void transform(String xslFileName, String xmlFileName) {
    try {
      File stylesheet = new File("src/" + xslFileName);
      TransformerFactory tFactory = TransformerFactory.newInstance();
      StreamSource styleSource; // Use a Transformer for output

      // hello hacky...
      if (stylesheet.exists()) {
        styleSource = new StreamSource(stylesheet);
      } else { // when in a JAR...
        InputStream styleSheetStream = getClass().getResourceAsStream(xslFileName);
        styleSource = new StreamSource(styleSheetStream);
      }

      Transformer transformer = tFactory.newTransformer(styleSource);
      DOMSource source = new DOMSource(xmlDoc);
      StreamResult result = new StreamResult(new FileOutputStream(xmlFileName));

      transformer.transform(source, result);
    } catch (TransformerConfigurationException tce) {
      // Error generated by the parser
      System.out.println("\n** Transformer Factory error");
      System.out.println("   " + tce.getMessage());

      // Use the contained exception, if any
      Throwable exception = tce;

      if (tce.getException() != null) {
        exception = tce.getException();
      }

      exception.printStackTrace();
    } catch (TransformerException te) {
      // Error generated by the parser
      System.out.println("\n** Transformation error");
      System.out.println("   " + te.getMessage());

      // Use the contained exception, if any
      Throwable exception = te;

      if (te.getException() != null) {
        exception = te.getException();
      }

      exception.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Parse document to string
   *
   * @return String
   */
  public String toString() {
    String parsedXmlDoc = "";

    try {
      TransformerFactory tf = TransformerFactory.newInstance();
      Transformer transformer = tf.newTransformer();
      transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
      StringWriter writer = new StringWriter();
      transformer.transform(new DOMSource(xmlDoc), new StreamResult(writer));
      parsedXmlDoc = writer.getBuffer().toString().replaceAll("\n|\r", "");

    } catch (Exception e) {
      e.printStackTrace();
    }
    return parsedXmlDoc;
  }
}
