package schmidt.billomat;

import javax.swing.*;
import java.io.*;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

/**
 * Ultra-simple Billomat customer exporter that also includes customer attributes and spits out Excel 2003 XML that
 * can be imported in Excel and OpenOffice/LibreOffice right away
 *
 * @author Rene Schmidt <rene@reneschmidt.de>
 */
public class CustomerExporter {
  /**
   * progress monitor
   */
  public JilloMateProgressMonitor pm;

  /**
   * Billomat request wrapper
   */
  public BillomatRequest bmReq;

  /**
   * Shut down the whole thing...
   *
   * @param returnCode Return code
   */
  public void shutdown(int returnCode) {
    System.out.println("Beende...");
    pm.close();
    bmReq.getConnectionManager().shutdown();
    System.exit(returnCode);
  }

  /**
   * Constructor
   */
  public CustomerExporter() {
    try {
      String apiKey = askApiKey();
      String billomatId = askBillomatId();

      if (apiKey == null) {
        throw new Exception("Kein Billomat-API-Key eingegeben");
      }

      pm = new JilloMateProgressMonitor(new JFrame(), "Bitte warten...", 0, 10);
      pm.setMillisToDecideToPopup(0);
      pm.setMillisToPopup(0);

      bmReq = new BillomatRequest(apiKey, billomatId);

      pm.setProgress(2);
      pm.setNote("Hole Kundendaten...");

      HttpResponse response = bmReq.doRequest("/api/clients", true);

      StatusLine statusLine = response.getStatusLine();
      int statusCode = statusLine.getStatusCode();

      switch (statusCode) {
        case 200:
          pm.setNote("Verarbeite Daten...");
          pm.setProgress(4);
          break;
        default:
          JOptionPane.showMessageDialog(
              null,
              statusLine,
              "JilloMate",
              JOptionPane.ERROR_MESSAGE
          );
          throw new Exception("Billomat-Server lieferte einen Fehler.");
      }

      BillomatDomDocument bmDoc = new BillomatDomDocument(bmReq.content);

      String xslFileName = "xml/generic_xml_to_excel2003_xml.xsl";

      File file = askFilePath("billomat_kundenexport.xml");
      String xmlFileName;

      if (file != null) {
        xmlFileName = file.getPath();
      } else {
        throw new Exception("Kein Dateiname zum Speichern angegeben");
      }

      pm.setNote("Transformiere XML...");
      pm.setProgress(5);

      bmDoc.parse();

      pm.setNewMaximum(bmDoc.clientNodes.getLength());

      bmDoc.enrichDocWithCustomerAttribs(bmReq, pm);
      bmDoc.transform(xslFileName, xmlFileName);

      pm.setNote("Fertig!");
      pm.setJilloMateProgress(pm.getMaximum());

      JOptionPane.showMessageDialog(
          null,
          "Datei gespeichert nach " + xmlFileName + ".\n" +
              "Sie können die Datei in MS-Office 2003+ und LibreOffice/OpenOffice 3.4+ öffnen."
      );

    } catch (Exception e) {
      System.out.println(e.getMessage());
      e.printStackTrace();
      shutdown(0);
    } finally {
      shutdown(0);
    }
  }

  /**
   * Main function, duh
   *
   * @param args main arguments
   */
  public static void main(String[] args) {
    new CustomerExporter();
  }

  /**
   * Open save file dialog
   *
   * @return File|null
   */
  public File askFilePath(String suggestFileName) {
    File newFile = null;
    JFileChooser fc = new JFileChooser();
    fc.setSelectedFile(new File(suggestFileName));

    int overwrite = JOptionPane.NO_OPTION;
    int retVal;

    boolean isTrue;

    do {
      retVal = fc.showSaveDialog(new JFrame());

      if (retVal == JFileChooser.APPROVE_OPTION) {
        newFile = fc.getSelectedFile();

        if (newFile.exists()) {
          overwrite = JOptionPane.showConfirmDialog(
              null,
              "Datei existiert schon. Überschreiben?",
              "JilloMate",
              JOptionPane.YES_NO_OPTION
          );
        }
      }
      isTrue = retVal == JFileChooser.APPROVE_OPTION && overwrite == JOptionPane.NO_OPTION && newFile.exists();
    } while (isTrue);

    if (retVal == JFileChooser.APPROVE_OPTION) {
      return newFile;
    }

    return null;
  }

  /**
   * Ask for Billomat API key. For security reasons, API key is not being cached right now.
   *
   * @return string|null
   */
  public String askApiKey() {
    return JOptionPane.showInputDialog(
        null,
        "Bitte Billomat-API-Key eingeben",
        "JilloMate Kundenexport",
        JOptionPane.QUESTION_MESSAGE
    );
  }

  /**
   * Ask for Billomat ID. For security reasons, it is not being cached right now.
   *
   * @return string|null
   */
  public String askBillomatId() {
    return JOptionPane.showInputDialog(
        null,
        "Bitte Billomat-ID eingeben",
        "JilloMate Kundenexport",
        JOptionPane.QUESTION_MESSAGE
    );
  }
}