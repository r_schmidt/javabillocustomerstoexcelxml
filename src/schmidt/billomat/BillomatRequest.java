package schmidt.billomat;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;
import java.net.URI;

/**
 * Do Billomat request
 */
public class BillomatRequest extends DefaultHttpClient {
  /**
   * httpGet request object
   */
  public HttpGet getRequest;

  /**
   * Content
   */
  public String content;

  /**
   * API key
   */
  public String apiKey;

  /**
   * Billomat ID
   */
  public String billomatId;

  /**
   * Constructor
   */
  public BillomatRequest(String apiKey, String billomatId) {
    this.apiKey = apiKey;
    this.billomatId = billomatId;
    getRequest = new HttpGet();
    getRequest.addHeader("Accept", "application/xml");
    getRequest.addHeader("X-BillomatApiKey", apiKey);
  }

  /**
   * Write file
   *
   * @param fileContent File content
   * @param fileName    File name
   * @param fileExt     File extension
   */
  public void writeFile(String fileContent, String fileName, String fileExt) {
    try {
      // Create temp file.
      File temp = File.createTempFile(fileName, fileExt);

      // Write to temp file
      BufferedWriter out = new BufferedWriter(new FileWriter(temp));
      out.write(fileContent);
      out.close();
    } catch (Exception e) {
      System.err.println("Error: " + e.getMessage());
    }
  }

  /**
   * Convert response to string
   *
   * @param response Response object
   * @return String
   */
  public String convertResponseToString(HttpResponse response) {
    String cont = "";

    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

      String line = "";

      while ((line = rd.readLine()) != null) {
        cont += line;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return cont;
  }

  /**
   * do request
   *
   * @param uri URI to call
   * @return HttpResponse
   */
  public HttpResponse doRequest(String uri, boolean writeToFile) {
    HttpResponse response = null;

    try {
      getRequest.setURI(new URI("https://" + billomatId + ".billomat.net/" + uri));
      response = execute(getRequest);
      content = convertResponseToString(response);

      if (writeToFile) {
        writeFile(content, "JilloMate.temp", "xml");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return response;
  }
}